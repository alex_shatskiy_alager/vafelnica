#include <opencv2/core/utility.hpp>
#include "opencv2/highgui.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/imgproc.hpp"

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <chrono>
#include <cmath>
#include <string>
#include <condition_variable>
#include <list>
#include <thread>
#include <future>
#include <dirent.h>

#define OPENCV
#include "../yolo_v2_class_old.hpp"

using namespace std;
using namespace cv;

//string img_folder_name = "recognition_camera_night";
//string img_folder_name = "recognition_camera_day";
//string img_folder_name = "recognition_camera_morning";
//string img_folder_name = "recognition_camera"; // -- Первый совместный тестовый датасет.
//string img_folder_name = "ti_2.2";
string img_folder_name = "ti_2";

int fr_w0, fr_h0, fr_w1;
int fr_h1 = 320; // -- Высота фрейма для работы.
int num_fr0 = 0; // -- Первый фрейм цикла.
int num_fr1 = 0; // -- Последний фрейм цикла (при значении 0 -- находится автоматически).

// -- xy - координаты точки середины данной рамки, wh - полуширина и полувысота рамки на текущем кадре с номером n,
// -- s - статус текущей рамки,
// -- h - номер кадра для рамки этого же трека, которая является предыдущей для данной рамки трека,
// -- i - номер предыдущей рамки в кадре с номером h.
struct xywh{Point2f xy; Point2f wh;};
struct xyn{Point2f xy; Point2f wh; int n;};
struct xyhi{Point2f xy; int h; int i;};
struct xyhnis{Point2f xy; Point2f wh; int h; int n; int i; int s;};
struct rcr{float x; float y; float w; float h;};

#define PQXX0
#ifdef PQXX0
   bool dump = 0; // -- Создавать, или нет дамп базы SQL.

//   string host_name = "192.168.77.247"; // -- Адрес хоста базы данных.
//   string db_name = "db_ti_2"; // -- Название бызы данных.
//   string user_name = "user"; // -- Имя пользователя базы данных.

   string host_name = "127.0.0.1"; // -- Адрес хоста базы данных.
   string db_name = "example"; // -- Название бызы данных.
   string user_name = "postgres"; // -- Имя пользователя базы данных.

   bool delet = 1; // -- Удалять, или нет таблицы SQL перед заполнением.
   bool trak2sql = 1; // -- Сохранение треков в базу SQL.
  #include "../sql_work_arch.hpp";
#endif

string img_way = "/home/alex/QtProg/"+img_folder_name; // -- Путь к папке с картинками.
bool show_tracs = 1; // -- Показывать, или нет треки после записи в базу.

int cs = 3; // -- Вариант загрузки нейросети:
struct yolo{string cfg; string weights; float tresh;};
vector<yolo> vy = {
 {"../../yolo_data/tiny1/tiny_my_cars1.cfg", "../../yolo_data/tiny1/weights/tiny_my_cars1_19744.weights", 0.2}, // cs=0 (yolo-tiny, обученное на авто с камеры GRZ и добавочно на грузовиках и автобусах из coco);
 {"../../yolo_data/yolo_v3/yolov3.cfg", "../../yolo_data/yolo_v3/weights/yolov3.weights", 0.5}, // cs=1 (обычное yolo3);
 {"../../yolo_data/y3_mod1/yolov3_mod1.cfg", "../../yolo_data/y3_mod1/weights/yolov3_mod1_15957.weights", 0.4}, // cs=2 (yolo3, обученное на авто с камеры GRZ и добавочно на грузовиках и автобусах из coco).
 {"../../yolo_data/tiny2/tiny_my_cars2.cfg", "../../yolo_data/tiny2/weights/tiny_my_cars2_15368.weights", 0.2}, // cs=3 (yolo-tiny, обученное на авто в одном классе).
 {"../../yolo_data/yolo_v4/yolo_v4.cfg", "../../yolo_data/yolo_v4/weights/yolo_v4_15244.weights", 0.2} // cs=4 (yolo3, обученное на авто в одном классе).
};



// ------------------------------------------------ Параметры для настройки алгоритма:
int d_hist_max = 4; // -- Максимальная глубина истории потока, глубже которой история потока удаляется.
float f2_min = 0.2; // -- Минимальная величина квадрата потока, ниже которой поток считается нулевым.
float k_mid_f = 1.2; // -- Коэффициент удлиннения вектора потока.
float alpha1_wh = 1; // -- Степень изменения размеров рамки для вычисления потка внутри нее.
float cross = 0.6; // -- Максимально-допустимая относительная разница пересечения площадей от рамок yolo, больше которой рамки считаются дублированные.
float y_0 = 0.35; // -- Минимальное значение координаты y центра рамки, при котором она добавляется как начальная точка (в единицах fr_h1).
float y_1 = 0.5; // -- Максимальное значение координаты y центра рамки, при котором она добавляется как конечная точка (статус -1) (в единицах fr_h1).
float koef_h = 0.7; // -- Минимальное значение высоты рамки, по отношению к ее ширине, при котором авто считается двигающимся вертикально.
int trac_min = 5; // -- Минимальная длина трека.
bool show = 1; // -- Показывать, или нет текущий процесс.
int fr_h_show = 960; // -- Высота фрейма при показе.
float y_viol = 0.37; // -- Минимальное значение координаты y центра рамки, при котором авто может быть нарушителем (в единицах fr_h1).
float dxy_stop = 0.02; // -- Относительный допуск по координатам, при котором авто считается в отдной точке (остановка).

float x_rel_max = 0.65; // -- Безразмерная x-координата верхней точки для построения красной линии отсечения рамок (потока машин в противоположном направлении)
float y_rel_min = 0.55; // -- Безразмерная y-координата правой точки ... (в единицах высоты фрейма).
int x_r_max, y_r_min;
Point2i p_top, p_right, p_vec;
float y_min0 = 0.15; // -- Минимальная безразмерная y-координата для рамки.
float y_max0 = 0.95; // -- Максимальная безразмерная y-координата для рамки.
float verify_rct_wh_max = 0.03; // -- Максимально-допустимое относительное отклонение размеров соседних рамок на траектории.
Point2f direct(0, 1); // -- Направление допустимого оптического потока.
float verify_rct_xy_max = 0.3;  // -- Максимально-допустимое относительное отклонение вектора между соседними рамками от вектора direct на траектории.

// ------------ Служебные параметры (не для настройки алгоритма):
int d_hist = 1; // -- Диапазон фреймов в течение которого показывется текущий трек.
int show_hist = 40; // -- Количество последних фреймов в течение которых учитываются точки на треках.
int show_hist2 = 10; // -- Количество последних фреймов в течение которых рисуются точки на видео.
int N_reserve = 150000; // -- Для резервирования памяти у векторов глобального цикла.
int reserv = 200; // -- Для резервирования памяти у векторов.
bool first_fr = 1; // -- Признак первого кадра.
int n_mem = N_reserve/1000; // -- Количество фреймов, после которого определяется остаток памяти.
float free_mem;

Scalar red = Scalar(0, 0, 255);
Scalar blue = Scalar(255, 0, 0);
Scalar green = Scalar(0, 255, 0);
Scalar white = Scalar(255, 255, 255);
Scalar black = Scalar(0, 0, 0);
Scalar viol = Scalar(255, 0, 255);
Scalar cyan = Scalar(255, 255, 0);
Scalar yell = Scalar(0, 255, 255);
float tresh;
int y_begin, y_end;
float k_show;
Size sz_show;


vector<string> split(string& str, const string& delim)
{
    vector<string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == string::npos) pos = str.length();
        string token = str.substr(prev, pos-prev);
        if (!token.empty()) tokens.push_back(token);
        prev = pos + delim.length();
    }
    while (pos < str.length() && prev < str.length());
    return tokens;
}

float GetFreeMemory()
{
    FILE* stream = popen( "vmstat -s", "r" );
    ostringstream output;
    while(!feof(stream) && !ferror(stream))
    {
        char buf[128];
        int bytesRead = fread( buf, 1, 128, stream );
        output.write( buf, bytesRead );
    }
    string s1 = output.str();
    vector<string> vs = split(s1, " ");
    //cout << endl << "0=" << vs[0] << "; 4=" << vs[4] << endl;
    float fmem = 0.000001*(stoi(vs[0])-stoi(vs[4]));
    return fmem;
} // -- END MarkGetFreeMemory


bool FileIsExist(string& filePath)
{
    bool isExist = false;
    ifstream fin(filePath.c_str());
    if (fin.is_open()) { isExist = true; }
    fin.close();
    return isExist;
} // -- END FileIsExist



vector<string> dir_content(string& fold, int typ)
{
    vector<string> dc0;
    DIR *dir;
    struct dirent *ent;
    if((dir = opendir(fold.c_str())) != NULL)
    {
      while((ent = readdir(dir)) != NULL)
      {
        string fname = string(ent->d_name);
        // -- typ = 4 (folder), typ = 8 (file).
        if(ent->d_type == typ && fname != "." && fname != ".."){dc0.emplace_back(fname);}
      } // -- END while ((ent = readdir (dir)) != NULL)
      closedir (dir);
    } // -- END if((dir = opendir(way)) != NULL)
    return dc0;
} // -- END dir_content




void print_Pt_hist(int n_hist1, vector<vector<xyhnis>>& Pt_hist, Mat& fr, vector<xywh>& vr, vector<vector<rcr>>& vrcr)
{
   int n_hist0 = n_hist1-show_hist2; if(n_hist0<0){n_hist0=0;}
   for(int i_hist=n_hist0; i_hist<n_hist1; i_hist++)
   {
       for(int i=0; i<Pt_hist[i_hist].size(); ++i)
       {
           xyhnis& p_now = Pt_hist[i_hist][i];
           Point2f p1 = k_show*p_now.xy;
           xyhnis& p_old = Pt_hist[p_now.h][p_now.i];
           Point2f p0 = k_show*p_old.xy;
           if(p_now.h != i_hist){line(fr, p0, p1, blue, 2);}
           Scalar clr = green;
           if(p_now.h == p_now.n){clr = red;}
           if(p_now.s == 0 && p_now.xy.y>y_end){clr = yell;}
           circle(fr, p1, 4, clr, -1);
       } // -- END // -- for(int i=0; i<Pt.size(); ++i)
   } // -- END for(int i_hist=1; i_hist<=n_hist; i_hist++)

   for(int i=0; i<vr.size(); ++i)
   {
      xywh& r = vr[i]; Point2i xy = k_show*(r.xy-r.wh); Point2i wh = 2*k_show*r.wh;
      Rect r_sh = Rect(xy.x, xy.y, wh.x, wh.y);
      rectangle(fr, r_sh, green, 2);
   } // -- END for(int i=0; i<sz0; ++i)

   for(int i0=0; i0<vrcr.size(); ++i0)
   {
       for(int i1=0; i1<vrcr[i0].size(); ++i1)
       {
           rcr& r = vrcr[i0][i1];
           rectangle(fr, Rect(k_show*r.x, k_show*r.y, k_show*r.w, k_show*r.h), yell, 2);
       } // -- END for(int i1=0; i1<vrcr[i0].size(); ++i1)
   } // -- END for(int i0=0; i0<vrcr.size(); ++i0)
} // -- END print_Pt_hist




void get_tracs(int n_hist_tot, vector<vector<xyhnis>>& Pt_hist, vector<long>& hist2ms)
{
   vector<vector<xyn>> Tracs; Tracs.reserve(N_reserve);
   for(int h=Pt_hist.size()-1; h>-1; --h)
   {
      for(int i=0; i<Pt_hist[h].size(); ++i)
      {
          xyhnis& Pt0 = Pt_hist[h][i];
          if(Pt0.s == 0 && Pt0.h != Pt0.n)
          {
              vector<xyn> Trac0; Trac0.reserve(reserv);
              Trac0.emplace_back(xyn{Pt0.xy, Pt0.wh, Pt0.n});
              int h1 = Pt0.h; int n1 = Pt0.n; int s1 = Pt0.s; int i1 = Pt0.i;
              Pt0.s = -1;
              while(s1 != -1 && h1 != n1)
              {
                  xyhnis& Pt1 = Pt_hist[h1][i1];
                  h1 = Pt1.h; n1 = Pt1.n; s1 = Pt1.s; i1 = Pt1.i;
                  Pt1.s = -1;
                  Trac0.emplace_back(xyn{Pt1.xy, Pt1.wh, Pt1.n});
              } // -- END while(s1 != -1 && h1 != n1)

              if(Trac0.size()>trac_min && Trac0[0].xy.y>y_end && Trac0[Trac0.size()-1].xy.y<y_begin)
              {
                  KalmanFilter KF(4, 2, 0);
                  Mat_<float> measurement(2,1);

                  vector<xyn> Trac; Trac.reserve(Trac0.size());
                  bool first = 1;
                  for(int k=Trac0.size()-1; k>-1; --k)
                  {
                      xyn& Tr0 = Trac0[k];
                      Point2f& xy = Tr0.xy;
                      if(first && xy.y<y_begin){continue;}
                      if(first)
                      {
                          first = 0;
                          KF.transitionMatrix = Mat_<float>(4, 4) << (1,0,1,0,   0,1,0,1,  0,0,1,0,  0,0,0,1);
                          measurement = Mat_<float>::zeros(2,1);
                          measurement.at<float>(0, 0) = xy.x;
                          measurement.at<float>(1, 0) = xy.y;
                          KF.statePre.setTo(0);
                          KF.statePre.at<float>(0, 0) = xy.x;
                          KF.statePre.at<float>(1, 0) = xy.y;
                          KF.statePost.setTo(0);
                          KF.statePost.at<float>(0, 0) = xy.x;
                          KF.statePost.at<float>(1, 0) = xy.y;
                          setIdentity(KF.transitionMatrix);
                          setIdentity(KF.measurementMatrix);
                          setIdentity(KF.processNoiseCov, Scalar::all(.005));
                          setIdentity(KF.measurementNoiseCov, Scalar::all(1e-1));
                          setIdentity(KF.errorCovPost, Scalar::all(.1));
                      }
                      else
                      {
                          Mat prediction = KF.predict();
                          Point2f predictPt(prediction.at<float>(0), prediction.at<float>(1));
                          measurement(0) = xy.x;
                          measurement(1) = xy.y;
                          //Point2f measPt(measurement(0), measurement(1));
                          Mat estimated = KF.correct(measurement);
                          Point2f statePt(estimated.at<float>(0), estimated.at<float>(1));
                          xy = statePt;
                      } // -- END if(!first)
                      Trac.emplace_back(Tr0);
                  } // -- END for(int k=Trac0.size()-1; k>-1; --k)

                  if(Trac.size()>trac_min){Tracs.emplace_back(Trac);}
              } // -- END if(Trac0.size()>trac_min && Trac0[0].xy.y>y_end && Trac0[Trac0.size()-1].xy.y<y_begin)
           } // -- END if(Pt.s == 0 && Pt.h != Pt.n)
       } // -- END for(int i=0; i<Pt_hist[h].size(); ++i)
   } // -- END for(int h=Pt_hist.size()-1; h>0; --h)
   Pt_hist.clear();
   cout << endl << "Find " << Tracs.size() << " Tracs" << endl;
   #ifdef PQXX0
      // ------------------------------------------------------- save to sql
      if(trak2sql){SQL_WORK sw; sw.put_trak2sql(Tracs, hist2ms);}
   #endif
   // ------------------------------------------------------- show tracs
   if(show_tracs)
   {
      Mat fr_trac = Mat(sz_show, CV_8UC3); fr_trac = black;
      char key = 0; int i_hist = 0;
      int ms = 0; int zn = 1;
      while(key != 27)
      {
         Mat fr = fr_trac.clone();
         int trac_col = 0;
         for(int i_tr=0; i_tr<Tracs.size(); ++i_tr)
         {
             vector<xyn>& Trac = Tracs[i_tr];
             int tsz = Trac.size(); int tsz_1 = tsz-1;
             //cout << i_tr << "/" << Tracs.size() << "; tsz=" << tsz << endl;
             if(Trac[0].n<=i_hist && Trac[0].n>i_hist-d_hist)
             {
                trac_col = Tracs.size()-i_tr;
                for(int k=1; k<tsz; ++k)
                {
                    xyn& Tr1 = Trac[k];
                    xyn& Tr0 = Trac[k-1];
                    Point2f p1 = k_show*Tr1.xy;
                    Point2f wh1 = k_show*Tr1.wh;
                    Point2f p0 = k_show*Tr0.xy;
                    Point2f wh0 = k_show*Tr0.wh;
                    if(k==1)
                    {
                        circle(fr, p0, 4, red, -1);
                        Rect rct0 = Rect(p0.x-wh0.x, p0.y-wh0.y, 2*wh0.x, 2*wh0.y);
                        //rectangle(fr, rct0, yell, 2);
                    } // -- END if(k==1)
                    Scalar clr = green; if(k==tsz_1){clr = yell;}
                    circle(fr, p1, 4, clr, -1);
                    Rect rct1 = Rect(p1.x-wh1.x, p1.y-wh1.y, 2*wh1.x, 2*wh1.y);
                    //rectangle(fr, rct1, clr, 2);
                    line(fr, p0, p1, green, 1, 1);
                 } // -- END for(int k=1; k<Trac.size(); ++k)
             } // -- END if(Trac.size()>1)
         } // -- END for(int i_tr=0; i_tr<Tracs.size(); ++i_tr)

         string txt = "Hist " + to_string(i_hist) + "/" + to_string(n_hist_tot);
         if(trac_col){txt += "; Trac " + to_string(trac_col) + "/" + to_string(Tracs.size());}
         putText(fr, txt, Point(10, 40), FONT_ITALIC, 1, green, 1, 4);
         imshow(img_folder_name, fr); moveWindow(img_folder_name, 10, 40);

         if(trac_col){ms = 0;}
         else{ms = 1;}
         key = waitKey(ms);
         if(key == 27){break;}
         else if(key == 'n'){zn=-1;}
         else if(key == 'm'){zn=1;}

         if(zn==1 && i_hist<n_hist_tot){i_hist += zn;}
         if(zn==-1 && i_hist>0){i_hist += zn;}
      } // -- END while(key != 27)
   } // -- END if(show_tracs)
} // -- END get_tracs




bool verify_rct(Point2f& wh0, Point2f& wh1, Point2f& xy0, Point2f& xy1)
{
    Point2f m = wh1-wh0; Point2f p = wh1+wh0;
    if(m.x*m.x+m.y*m.y > verify_rct_wh_max*(p.x*p.x+p.y*p.y)){return 0;}
    Point2f v_rct = xy1-xy0;
    if(v_rct.x*direct.x+v_rct.y*direct.y<0 && v_rct.x*v_rct.x+v_rct.y*v_rct.y>verify_rct_xy_max*wh0.x*wh0.y){return 0;}
    return 1;
} // -- END verify_rct




void verify_and_add_new_pt(int n_hist, vector<xywh>& vr, vector<vector<xyhnis>>& Pt_hist, list<xyhi>& fl_prev, Mat& fr)
{
    // -- Проверка точек-кандидатов и добавление их в историю
    int hist_min = n_hist-d_hist_max;
    list<xyhi>::iterator it; list<xyhi>::iterator it_min;

    for(it=fl_prev.begin(); it!=fl_prev.end(); it++){if((*it).h<hist_min){it=fl_prev.erase(it);}} // -- удаление устаревших элементов оптического потока

    vector<bool> dubl(vr.size(), 0);
    vector<xyhnis> vPt; vPt.reserve(vr.size());
    for(int i=0; i<vr.size(); ++i)
    { // --------------------------- Цикл по всем рамкам yolo с индексом i.
        bool it_ok = 0;
        Point2f& xy = vr[i].xy;
        Point2f& wh = vr[i].wh;
        float dist2_min = wh.x*wh.x+wh.y*wh.y; // -- Устанавливаем начальное значение для минимума квадрата расстояния от элемента потока до рамки i.

        Point2f p1_ok;
        for(it=fl_prev.begin(); it!=fl_prev.end(); it++)
        { // ---------------------- Цикл по всем элементам оптического потока -- выбираем лучший для данной рамки i.
            xyhi& flhi = *it;
            xyhnis& Ptf = Pt_hist[flhi.h][flhi.i];
            Point2f& p0 = Ptf.xy; Point2f& fl = flhi.xy; Point2f p1 = p0+(n_hist-flhi.h)*fl;
            Point2f d = xy-p1;
            if(abs(d.x)<wh.x && abs(d.y)<wh.y)
            { // --- Условие на самый близкий к рамке i элемент потока it_min от рамки с предыдущего слоя, которая этот it_min и генерирует.
                float dist2 = d.x*d.x+d.y*d.y;
                if(dist2<dist2_min && Ptf.s != 1 && verify_rct(Ptf.wh, wh, p0, xy)){dist2_min = dist2; it_min = it; it_ok = 1; p1_ok = p1;}
            } // -- END if(abs(d.x)<wh.x && abs(d.y)<wh.y)
            if(show)
            {
               Point2f p_0 = k_show*p0; Point2f fl1 = k_show*fl;
               line(fr, p_0, k_show*p1, viol, 2);
               for(int j=0; j<n_hist-flhi.h; ++j){Point2f p3 = p_0+j*fl1; circle(fr, p3, 7, viol, -1);}
            } // -- END if(show)
        } // -- END for(it=fl_prev.begin(); it!=fl_prev.end(); it++)
        if(it_ok)
        { // ----- Если найден подходящий (лучший) элемент потока it_min для рамки i.
            xyhi& flhi = *it_min;
            xyhnis& Ptf = Pt_hist[flhi.h][flhi.i];
            int k_min = i;
            for(int k=0; k<vr.size(); ++k)
            { // ---------------------------- Еще раз пробегаемся по всем незанятым рамкам и ищем лучшее соответствие с it_min.
                if(dubl[k] || k==i){continue;}
                Point2f& vp_k = vr[k].xy;
                Point2f d = vp_k-p1_ok;
                float dist2 = d.x*d.x+d.y*d.y;
                if(dist2<dist2_min && Ptf.s != 1 && verify_rct(Ptf.wh, wh, Ptf.xy, xy)){dist2_min = dist2; k_min = k;}
            } // -- END for(int k=0; k<vp.size(); ++k)

            if(k_min == i)
            { // -- Если лучшее соответствие у it_min с текущей рамкой i:
                xyhnis p = {xy, wh, (*it_min).h, n_hist, (*it_min).i, 0}; vPt.emplace_back(p); // -- Добавляем в историю рамку i.
                Pt_hist[(*it_min).h][(*it_min).i].s = 1; // -- Ставим статус занятой для рамки с предыдущего слоя.
                fl_prev.erase(it_min); // -- Удаляем элемент потока it_min.
                dubl[i] = 1; // -- Помечаем рамку i как занятую.
            }
            else
            { // -- Если лучшее соответствие у it_min не с текущей рамкой i, а с рамкой k_min:
                xyhnis p = {vr[k_min].xy, wh, (*it_min).h, n_hist, (*it_min).i, 0};
                vPt.emplace_back(p); // -- Добавляем в историю рамку k_min.
                Pt_hist[(*it_min).h][(*it_min).i].s = 1; // -- Ставим статус занятой для рамки с предыдущего слоя.
                fl_prev.erase(it_min); // -- Удаляем элемент потока it_min.
                dubl[k_min] = 1; // -- Помечаем рамку k_min как занятую.
                i--; // -- Возвращаем индекс рамки i на предыдущую позицию -- для повторного поиска элемента потока для рамки i.
            } // -- END if(k_min != i)
        } // -- END if(it_ok)
    } // -- END for(int i=0; i<vp.size(); ++i)

    for(int i=0; i<vr.size(); ++i)
    { // -- Добавляем в историю все оставшиеся свободные рамки.
        if(!dubl[i])
        {
            Point2f& xy = vr[i].xy; Point2f& wh = vr[i].wh;
            xyhnis p = {xy, wh, n_hist, n_hist, i, 0};
            vPt.emplace_back(p);
            if(show)
            {
                Point xy1 = k_show*(xy-wh); Point wh1 = 2*k_show*wh;
                rectangle(fr, Rect(xy1.x, xy1.y, wh1.x, wh1.y), red, 1);
            } // -- END if(show)#include "QtTest/QTest"

        } // -- END if(!dubl[i])
    } // -- END for(int i=0; i<vr.size(); ++i)
    Pt_hist.emplace_back(vPt); // -- Добавление текущего вектора рамок к тензору истории рамок.
} // -- END verify_and_add_new_pt




void get_fl(int n_hist, const Mat& flow, vector<xywh>& vr, list<xyhi>& fl_prev, vector<vector<rcr>>& vrcr)
{
   // -------------------------- Добавление элементов потока в fl_prev для каждой из новых рамок vr в yolo.
   for(int i=0; i<vr.size(); ++i)
   {
      xywh& r = vr[i]; vector<rcr>& rcr_i = vrcr[i];
      float w = alpha1_wh*r.wh.x; float h = alpha1_wh*r.wh.y;
      int x0 = r.xy.x - w; if(x0<0){x0 = 0;}
      int y0 = r.xy.y - h; if(y0<0){y0 = 0;}
      int x1 = x0 + 2*w; if(x1>=flow.cols){x1 = flow.cols-1;}
      int y1 = y0 + 2*h; if(y1>=flow.rows){y1 = flow.rows-1;}
      int n=0; Point2f mid_f = Point2f(0, 0);
      for(int y = y0; y<y1; ++y)
      {
         for(int x = x0; x<x1; ++x)
         {
            bool ok = 1;
            for(int k=0; k<rcr_i.size(); ++k)
            {
               rcr& rcr_ik = rcr_i[k];
               if(x>rcr_ik.x && x<rcr_ik.x+rcr_ik.w && y>rcr_ik.y && y<rcr_ik.y+rcr_ik.h){ok = 0; break;}
            } // -- END for(int k=0; k<rcr_i.size(); ++k)

            if(ok)
            {
               const Point2f& f = flow.at<Point2f>(y, x);
               if(f.x*f.x+f.y*f.y>f2_min){mid_f += f; n++;}
            } // -- END if(ok)
         } // -- END for(int x = x0; x<x1; ++x)
      } // -- END for(int y = y0; y<y1; ++y)

      Point2f mid_fl = Point2f(0, 0);
      if(n){mid_fl = mid_f*k_mid_f/n;}
      if(mid_fl.x*direct.x+mid_fl.y*direct.y<0){mid_fl = Point2f(0, 0);}
      fl_prev.push_back({mid_fl, n_hist, i});
   } // -- END for(int i=0; i<sz_vp; ++i)
} // -- END get_fl



bool get_trajectories(int n_hist, const Mat& flow, vector<xywh>& vr, Mat& fr, vector<vector<xyhnis>>& Pt_hist, list<xyhi>& fl_prev, vector<vector<rcr>>& vrcr)
{
    if(first_fr)
    {
        first_fr = 0;
        vector<xyhnis> vPt; vPt.reserve(vr.size());
        for(int i=0; i<vr.size(); i++){xyhnis p = {vr[i].xy, vr[i].wh, n_hist, n_hist, i, 0}; vPt.emplace_back(p);}
        Pt_hist.emplace_back(vPt);
    }
    else
    {
       verify_and_add_new_pt(n_hist, vr, Pt_hist, fl_prev, fr); // -- Проверка точек-кандидатов и добавление их в историю.
    }
    get_fl(n_hist, flow, vr, fl_prev, vrcr); // -- Добавление векторов потока в fl_prev для каждой из новых рамок vr в yolo.
    return 1;
} // -- END get_trajectories


vector<bbox_t> thr_yolo(Detector * det, Mat& fr, float tresh)
{
    return det->detect(fr, tresh);
} // -- END thr_yolo




int main(int argc, char* argv[])
{
   Detector * p_darknet = new Detector(vy[cs].cfg, vy[cs].weights);
   //Detector darknet(vy[cs].cfg, vy[cs].weights);
   tresh = vy[cs].tresh;
   vector<string> img_names;
   vector<long> hist2ms; int img_sz;

   img_way += "/";
   img_names = dir_content(img_way, 8);
   sort(img_names.begin(), img_names.end());
   img_sz = img_names.size();
   N_reserve = img_sz;
   n_mem = N_reserve/900;

   hist2ms.resize(img_sz);
   for(int num_fr=0; num_fr<img_sz; ++num_fr)
   {
       string& im0 = img_names[num_fr];
       string im1 = im0.substr(0, im0.length()-4);
       hist2ms[num_fr] = stol(im1);
   } // -- END for(int num_fr=0; num_fr<img_sz; ++num_fr)

   Mat img0 = imread(img_way+img_names[img_names.size()/2]);
   fr_w0 = img0.cols; fr_h0 = img0.rows;
   long ms0 = hist2ms[0]; long ms1 = hist2ms[img_sz-1];
   cout << "img_sz=" << img_sz << "; ms0=" << ms0 << "; ms1=" << ms1;
   float fps_midl = (float)img_sz*1000.f/(ms1-ms0);
   cout << "; fps_midl=" << fps_midl << endl;

   int img_sz_1 = img_sz-1;

   chrono::duration<double> start_0;
   chrono::system_clock::time_point start_new, start_old;
   float dtime;

   // -- fr_w1 и fr_h1 - пропорции рабочей картинки для yolo и оптического потока.
   fr_w1 = round((float)fr_h1*fr_w0/fr_h0);
   k_show = (float)fr_h_show/fr_h1;
   y_begin = round(y_0*fr_h1); y_end = round(y_1*fr_h1);
   int y_begin_show = round(k_show*y_0*fr_h1); int y_end_show = round(k_show*y_1*fr_h1);

   Size sz1 = Size(fr_w1, fr_h1);
   cout << "fr_w0=" << fr_w0  << "; fr_h0=" << fr_h0 << endl;
   cout << "fr_w1=" << fr_w1 << "; fr_h1=" << fr_h1 << endl;

   x_r_max = x_rel_max*fr_w1, y_r_min = y_rel_min*fr_h1;
   p_top = Point2i(x_r_max, 0); p_right = Point2i(fr_w1-1, y_r_min); p_vec = p_right-p_top;
   Point2i p_top_show = k_show*p_top; Point2i p_right_show = k_show*p_right;
   int y_min1 = round(y_min0*fr_h1);
   int y_max1 = round(y_max0*fr_h1);
   int y_viol1 = round(y_viol*fr_h_show);
   int y_viol2 = round((y_viol+dxy_stop)*fr_h_show);
   float norm_direct = sqrt(direct.x*direct.x+direct.y*direct.y);
   if(norm_direct>0){direct /= norm_direct;}

   sz_show = Size(round(k_show*fr_w1), round(k_show*fr_h1));

   Mat frame1 = Mat(sz1, CV_8UC3); frame1 = black;
   Mat fr_show = Mat(sz_show, CV_8UC3); fr_show = black;
   Mat flow = Mat(sz1, CV_32FC1);
   Mat prev = Mat(sz1, CV_8UC1); prev = Scalar(0);
   Mat next = Mat(sz1, CV_8UC1); next = Scalar(0);
   Mat frame0;

   vector<vector<xyhnis>> Pt_hist; Pt_hist.reserve(N_reserve);
   list<xyhi> fl_prev;

   start_new = chrono::system_clock::now();
   char key = 0; bool pause = 0; int n_hist = 0;
   vector<bbox_t> vbb; vbb.reserve(reserv);
   vector<xywh> vr; vr.reserve(reserv);
   vector<rcr> vrcr0; vrcr0.reserve(reserv);
   vector<vector<rcr>> vrcr; vrcr.reserve(reserv);
   int i_mem = 0; int imem = 1; free_mem = GetFreeMemory();

   if(num_fr1<=num_fr0){num_fr1=img_sz;}
   for(int num_fr=num_fr0; num_fr<num_fr1; ++num_fr)
   {
      string im_way = img_way+img_names[num_fr];
      try
      {
         frame0 = imread(im_way);
         if(!frame0.data){cout << endl << "num_fr=" << num_fr << "; bad 1 img=" << im_way << endl; continue;}
      }
      catch(Exception& ex)
      {cout << endl << "num_fr=" << num_fr << "; bad 2 img=" << im_way << endl; continue;}

      resize(frame0, frame1, sz1);
      //vbb = darknet.detect(frame1, tresh);
      future<vector<bbox_t>> thr_yolo_get = async(launch::async, thr_yolo, p_darknet, ref(frame1), tresh);

      prev = next.clone();
      cvtColor(frame1, next, COLOR_BGR2GRAY);
      calcOpticalFlowFarneback(prev, next, flow, 0.5, 3, 15, 3, 5, 1.2, 0);

      vector<Rect> vr_0; vr_0.reserve(vbb.size());
      for(int i=0; i<vbb.size(); ++i)
      {
         bbox_t& r = vbb[i];
         Point2i vec = Point2i(r.x, r.y)-p_top;
         if(p_vec.x*vec.y-p_vec.y*vec.x>0 && r.y>y_min1 && r.y+r.h<y_max1){vr_0.emplace_back(Rect(r.x,r.y,r.w,r.h));}
      } // -- END for(int i=0; i<vbb.size(); ++i)

      int sz_0 = vr_0.size();
      if(show)
      {
          line(fr_show, Point(0, y_begin_show), Point(fr_show.cols-1, y_begin_show), cyan, 2);
          line(fr_show, Point(0, y_end_show), Point(fr_show.cols-1, y_end_show), cyan, 2);
          line(fr_show, p_top_show, p_right_show, red, 2);
          //line(fr_show, Point(0, y_viol1), Point(fr_show.cols-1, y_viol1), red, 2);
          //line(fr_show, Point(0, y_viol2), Point(fr_show.cols-1, y_viol2), red, 2);
          for(int i=0; i<sz_0; ++i)
          {
             Rect& r0 = vr_0[i];
             Rect r_sh = Rect(k_show*r0.x, k_show*r0.y, k_show*r0.width, k_show*r0.height);
             rectangle(fr_show, r_sh, viol, 1);
          } // -- END for(int i=0; i<sz0; ++i)
      } // -- END if(show)

      vr.clear();
      vrcr.clear();
      for(int i1=0; i1<sz_0; i1++)
      {
         vrcr0.clear();
         Rect& r1 = vr_0[i1];
         Point2f wh1 = 0.5*Point2f(r1.width,r1.height);
         if(wh1.y<koef_h*wh1.x){continue;}
         xywh vr1 = {Point2f(r1.x,r1.y)+wh1, wh1};
         float S1 = r1.width*r1.height;
         bool ok = 1;
         for(int i2=0; i2<sz_0; ++i2)
         {
            if(i1==i2){continue;}
            Rect& r2 = vr_0[i2];
            float l_s = MAX(r1.x, r2.x);
            float r_s = MIN(r1.x+r1.width, r2.x+r2.width); if(r_s<l_s){continue;}
            float t_s = MAX(r1.y, r2.y);
            float d_s = MIN(r1.y+r1.height, r2.y+r2.height); if(d_s<t_s){continue;}
            float w_s = r_s-l_s;
            float h_s = d_s-t_s;
            float S_cross = w_s*h_s;
            float S2 = r2.width*r2.height;
            float S_min = MIN(S1, S2);
            // ---------------------------------------- Проверка на дублированность рамок для одного объекта.
            if(S_cross > cross*S_min)
            {
                if(S1 == S_min){ok = 0; break;}
            }else{vrcr0.emplace_back(rcr{l_s, t_s, w_s, h_s});}
         } // -- END for(int i2=0; i2<sz_0; ++i2)
         if(ok){vr.emplace_back(vr1); vrcr.emplace_back(vrcr0);}
      } // -- END for(int i1=0; i1<sz_0; i1++)

      get_trajectories(n_hist, flow, vr, fr_show, Pt_hist, fl_prev, vrcr);

      n_hist++;
      if(show)
      {
         print_Pt_hist(n_hist, Pt_hist, fr_show, vr, vrcr);
         start_old = start_new;
         start_new = chrono::system_clock::now(); start_0 = start_new-start_old;
         dtime = start_0.count();
         float fps_real = 1.f/dtime;
         string txt1 = "fps=" + to_string(fps_real) + "; fr=" + to_string(num_fr)+"/"+to_string(img_sz_1);
         putText(fr_show, txt1, Point(10, 40), FONT_ITALIC, 0.5, green, 1, 4);

         imshow(img_folder_name, fr_show); if(num_fr<num_fr0+2){moveWindow(img_folder_name, 10, 40);}
         resize(frame0, fr_show, sz_show);

         if(pause){key = waitKey(0);}
         else{key = waitKey(1);}

         if(key == 32){pause = 1;}
         else{pause = 0;}
         if(key==27){break;}
         // ------------------ END if(show)
      }

      cout << "num_fr=" << num_fr;
      i_mem++; if(i_mem>n_mem){i_mem=0; imem++; free_mem = GetFreeMemory();} cout << "; imem=" << imem << "; free mem=" << free_mem;
      cout << endl;
      if(free_mem<1.f){cout << endl << "OUT OF free mem!" << endl; break;}

      vbb = thr_yolo_get.get();
   } // -- END for(int num_fr=num_fr0; num_fr<num_fr1; ++num_fr)
   if(show){destroyWindow(img_folder_name);}
   delete p_darknet;
   get_tracs(n_hist, Pt_hist, hist2ms); // -- Добавление векторов траекторий.

   cout << endl << "END main" << endl;
   return 0;
} // -- END main
