QT += core gui
TEMPLATE = app
CONFIG -= app_bundle
CONFIG += c++17
CONFIG  += X11
CONFIG += console
TARGET = vafelnica

#opencv
include("../ubuntu_oc_contrib.pro")

LIBS += -L/home/alex/QtProg/darknet-new -l darknet_cpu_mp \

SOURCES += vafelnica.cpp \

HEADERS += sql_work_arch.hpp
