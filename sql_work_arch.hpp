#include <pqxx/pqxx>
using namespace pqxx;
using namespace std;

class SQL_WORK
{
   public:
   bool put_trak2sql(vector<vector<xyn>>& Tracs, vector<long>& hist2ms)
   {
       cout << endl << "BEGIN put_trak2sql" << endl;
       try
       {
         string connection_string = "host="+host_name+" dbname="+db_name+" user="+user_name;
         connection Con(connection_string.c_str());
         if(Con.is_open()){cout << "DBname= "+db_name+" Exist" << endl;}
         else{cout << "DBname= "+db_name+" NOT Exist!" << endl; return 1;}

         string tab1 = "data_sets"; vector<string> v1 = {"data_set_id", "data_set_name", "data_set_path"};
         string tab2 = "trajectories"; vector<string> v2 = {"trj_id", v1[0], "alg_id"};
         string tab3 = "trajectory_points"; vector<string> v3 = {"pnt_id", v2[0], "x", "y", "time_stamp_ms", "pnt_num"};
         string tab4 = "point_attribute_rectangle"; vector<string> v4 = {"pnt_attr_id", v3[0], "w", "h"};
         int key1_2 = 1; // -- alg_id
         int key3_0 = 1; // -- pnt_attr_id
         int key4_0 = 1; // -- data_set_id
         string data_set_name = img_folder_name;
         string data_set_path = "../";
         nontransaction work(Con);

         Con.prepare("deltab1", "DROP TABLE "+tab1+" CASCADE;");
         string t1 = "CREATE TABLE "+tab1+"("+v1[0]+" SERIAL PRIMARY KEY, "+v1[1]+" VARCHAR(255), "+v1[2]+" VARCHAR(255));";
         Con.prepare("create_tab1", t1);
         Con.prepare("check1", "select exists(select * from information_schema.tables where table_name='"+tab1+"')::int;");
         result res1 = work.exec_prepared("check1");
         if((*res1.begin()).at(0).as<int>())
         {
             cout << "Table "+tab1+" EXIST" << endl; if(delet){work.exec_prepared("deltab1"); work.exec_prepared("create_tab1");}
         }
         else{cout << "Table "+tab1+" NOT EXIST & CREATE" << endl; work.exec_prepared("create_tab1");}
         Con.prepare("insert1", "INSERT INTO "+tab1+"("+v1[0]+", "+v1[1]+", "+v1[2]+") VALUES (($1),($2),($3));");
         Con.prepare("select1", "SELECT "+v1[0]+" FROM "+tab1+" ORDER BY "+v1[0]+" DESC LIMIT 1;");
         result r1 = work.exec_prepared("select1");
         int d_set_id = 1; if(r1.size()){d_set_id += r1.begin().at(0).as<int>();}
         work.exec_prepared("insert1", d_set_id, data_set_name, data_set_path);

         Con.prepare("deltab2", "DROP TABLE "+tab2+" CASCADE;");
         string t2 = "CREATE TABLE "+tab2+"("+v2[0]+" SERIAL PRIMARY KEY, "+v2[1]+" int, "+v2[2]+" int";
         t2 += ", CONSTRAINT "+v2[1]+"_fk FOREIGN KEY ("+v2[1]+") REFERENCES "+tab1+"("+v1[0]+") MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION";
         t2 += ");";
         Con.prepare("create_tab2", t2);
         Con.prepare("check2", "select exists(select * from information_schema.tables where table_name='"+tab2+"')::int;");
         result res2 = work.exec_prepared("check2");
         if((*res2.begin()).at(0).as<int>())
         {
             cout << "Table "+tab2+" EXIST" << endl; if(delet){work.exec_prepared("deltab2"); work.exec_prepared("create_tab2");}
         }
         else{cout << "Table "+tab2+" NOT EXIST & CREATE" << endl; work.exec_prepared("create_tab2");}
         Con.prepare("insert2", "INSERT INTO "+tab2+"("+v2[1]+", "+v2[2]+") VALUES (($1),($2));");
         Con.prepare("select2", "SELECT "+v2[0]+" FROM "+tab2+" ORDER BY "+v2[0]+" DESC LIMIT 1;");

         Con.prepare("deltab3", "DROP TABLE "+tab3+" CASCADE;");
         string t3 = "CREATE TABLE "+tab3+" ("+v3[0]+" SERIAL PRIMARY KEY, "+v3[1]+" int, "+v3[2]+" double precision, "+v3[3]+" double precision, "+v3[4]+" bigint, "+v3[5]+" int";
         t3 += ", CONSTRAINT "+v3[1]+"_fk FOREIGN KEY ("+v3[1]+") REFERENCES "+tab2+"("+v2[0]+") MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION";
         t3 += ");";
         Con.prepare("create_tab3", t3);
         Con.prepare("check3", "select exists(select * from information_schema.tables where table_name='"+tab3+"')::int;");
         result res3 = work.exec_prepared("check3");
         if((*res3.begin()).at(0).as<int>())
         {
             cout << "Table "+tab3+" EXIST" << endl; if(delet){work.exec_prepared("deltab3"); work.exec_prepared("create_tab3");}
         }
         else{cout << "Table "+tab3+" NOT EXIST & CREATE" << endl; work.exec_prepared("create_tab3");}
         Con.prepare("insert3", "INSERT INTO "+tab3+"("+v3[1]+", "+v3[2]+", "+v3[3]+", "+v3[4]+", "+v3[5]+") VALUES (($1),($2),($3),($4),($5));");
         Con.prepare("select3", "SELECT "+v3[0]+" FROM "+tab3+" ORDER BY "+v3[0]+" DESC LIMIT 1;");

         Con.prepare("deltab4", "DROP TABLE "+tab4+" CASCADE;");
         string t4 = "CREATE TABLE "+tab4+"("+v4[0]+" SERIAL PRIMARY KEY, "+v4[1]+" int, "+v4[2]+" double precision, "+v4[3]+" double precision";
         t4 += ", CONSTRAINT "+v4[1]+"_fk FOREIGN KEY ("+v4[1]+") REFERENCES "+tab3+"("+v3[0]+") MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION";
         t4 += ");";
         Con.prepare("create_tab4", t4);
         Con.prepare("check4", "select exists(select * from information_schema.tables where table_name='"+tab4+"')::int;");
         result res4 = work.exec_prepared("check4");
         if((*res4.begin()).at(0).as<int>())
         {
             cout << "Table "+tab4+" EXIST" << endl; if(delet){work.exec_prepared("deltab4"); work.exec_prepared("create_tab4");}
         }
         else{cout << "Table "+tab4+" NOT EXIST & CREATE" << endl; work.exec_prepared("create_tab4");}
         Con.prepare("insert4", "INSERT INTO "+tab4+"("+v4[1]+", "+v4[2]+", "+v4[3]+") " "VALUES (($1),($2),($3));");

         for(int i_tr=Tracs.size()-1; i_tr>-1; --i_tr)
         {
             work.exec_prepared("insert2", key4_0, key1_2);
             result res2 = work.exec_prepared("select2");
             int tr_id = res2.begin().at(0).as<int>();
             vector<xyn>& Trac = Tracs[i_tr];
             for(int k=0; k<Trac.size(); ++k)
             {
                 int pnt_num = k;
                 xyn& Tr = Trac[k];
                 double x = (double)Tr.xy.x/fr_w1;
                 double y = (double)Tr.xy.y/fr_h1;
                 double w = (double)Tr.wh.x/fr_w1;
                 double h = (double)Tr.wh.y/fr_h1;
                 int ind = Tr.n - 1; if(ind<0){ind = 0;}
                 long ms = hist2ms[тгь_ак0 + ind];
                 //cout << "Tr.n=" << Tr.n << "; tr_id=" << tr_id << "; pnt_num=" << pnt_num << endl;
                 work.exec_prepared("insert3", tr_id, x, y, ms, pnt_num);
                 result res3 = work.exec_prepared("select3");
                 int pnt_id = res3.begin().at(0).as<int>();
                 work.exec_prepared("insert4", pnt_id, w, h);
             } // -- END for(int k=0; k<Trac.size(); ++k)
         } // -- END for(int i_tr=0; i_tr<Tracs.size(); ++i_tr)
         if(dump){string s = "pg_dump -d "+db_name+" -h "+host_name+" -U "+user_name+" > ../dump_"+img_folder_name+".sql"; system(s.c_str());}
       }
       catch (const sql_error& e)
       {
         cout << "SQL error: " << e.what();
         cout << " Query was: '" << e.query().c_str() << "'";
       }
       cout << "END put_trak2sql" << endl;
       return 1;
   } // -- END put_trak2sql
}; // -- END class SQL_WORK
