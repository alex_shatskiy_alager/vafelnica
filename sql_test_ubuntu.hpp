#include <pqxx/pqxx>
using namespace pqxx;

class SQL_TEST
{
   public:
   bool get_trak_from_sql(vector<vector<xywhtss>>& Tracs)
   {
       cout << endl << "BEGIN get_trak_from_sql" << endl;
       try
       {
         string connection_string = "host="+host_name+" dbname="+db_name+" user="+user_name;
         connection Con(connection_string);
         cout << "connect\n";
         if(Con.is_open()){cout << "DBname= "+db_name+" Exist" << endl;}
         else{cout << "DBname= "+db_name+" NOT Exist!" << endl; return 1;}
         
         if(dump){string s = "pg_dump -d "+db_name+" -h "+host_name+" -U "+user_name+" > ../dump_"+img_folder_name+".sql"; system(s.c_str());}


         if(test)
         {
             string tab = "trajectory_points";
             vector<string> v1 = {"pnt_id", "trj_id", "x", "y", "time_stamp_ms", "pnt_num"};
             nontransaction work(Con);

             Con.prepare("check1", "select exists(select * from information_schema.tables where table_name='"+tab+"')::int;");
             prepare::invocation res0 = work.prepared("check1");
             if((*(res0.exec()).begin()).at(0).as<int>()){cout << "Table "+tab+" EXIST" << endl;}
             else{cout << "Table "+tab+" NOT EXIST" << endl; return 0;}

             Con.prepare("sel1", "SELECT trj_id FROM "+tab+" WHERE pnt_num=0 ORDER BY trj_id;");
             prepare::invocation res1 = work.prepared("sel1");
             int tot_trj_num = res1.exec().size();
             cout << "tot_trj_num=" << tot_trj_num << endl;
             for(result::size_type trj_num=0; trj_num<tot_trj_num; ++trj_num)
             {
                 auto r1 = res1.exec();
                 int trj_id = r1[trj_num].at(0).as<int>();
                 //cout << "trj_id=" << trj_id << endl;
                 vector<xywhtss> Trac;
                 string select = "s_"+to_string(trj_num);
                 Con.prepare(select, "SELECT x, y, time_stamp_ms FROM "+tab+" WHERE trj_id="+to_string(trj_id)+" ORDER BY time_stamp_ms;");
                 prepare::invocation res2 = work.prepared(select);
                 int tot_pnt_num = res2.exec().size();
                 cout << "trj_num=" << trj_num << "; trj_id=" << trj_id << "; tot_pnt_num=" << tot_pnt_num << endl;
                 for(result::size_type pnt_num=0; pnt_num<tot_pnt_num; ++pnt_num)
                 {
                     auto r2 = res2.exec();
                     double x = r2[pnt_num].at(0).as<double>();
                     double y = r2[pnt_num].at(1).as<double>();
                     long ts = r2[pnt_num].at(2).as<long>();

                     xywhtss Tr = xywhtss{Point2f(x, y), Point2f(0.01, 0.01), ts, 0};
                     Trac.emplace_back(Tr);
                 } // -- END for(result::size_type pnt_num=0; pnt_num<res2.size(); ++pnt_num)

                 //vector<xywhtss> Trac;
                 Tracs.emplace_back(Trac);
              } // -- END for(result::size_type trj_num=0; trj_num<res1.size(); ++trj_num)
         }
         else
         { // ----------------------- if(!test)

            string tab1 = "trajectory_points";
            vector<string> v1 = {"pnt_id", "trj_id", "pnt_num", "x", "y", "time_stamp_ms"};
            string tab2 = "point_attribute_waffle_violation";
            vector<string> v2 = {"pnt_id", "is_violation"};
            nontransaction work(Con);

            Con.prepare("check1", "select exists(select * from information_schema.tables where table_name='"+tab1+"')::int;");
            prepare::invocation res1_1 = work.prepared("check1");
            if(res1_1.exec().size()){cout << "Table "+tab1+" EXIST" << endl;}
            else{cout << "Table "+tab1+" NOT EXIST" << endl; return 0;}

            Con.prepare("check2", "select exists(select * from information_schema.tables where table_name='"+tab2+"')::int;");
            prepare::invocation res2_1 = work.prepared("check2");
            if(res2_1.exec().size()){cout << "Table "+tab2+" EXIST" << endl;}
            else{cout << "Table "+tab2+" NOT EXIST" << endl; return 0;}

            Con.prepare("sel2", "SELECT pnt_id FROM "+tab2+" WHERE is_violation=true ORDER BY pnt_id;");
            prepare::invocation res2_2 = work.prepared("sel2");
            int tot_pnt_id_v = res2_2.exec().size();
            map<int, bool> mp;
            for(result::size_type pnt_id_v=0; pnt_id_v<tot_pnt_id_v; ++pnt_id_v)
            {
                auto r2 = res2_2.exec();
                int pnt_id = r2[pnt_id_v].at(0).as<int>();
                mp[pnt_id] = 1;
            } // -- END for(result::size_type pnt_id_v=0; pnt_id_v<tot_pnt_id_v; ++pnt_id_v)

            Con.prepare("sel1", "SELECT trj_id FROM "+tab1+" WHERE pnt_num=0 ORDER BY trj_id;");
            prepare::invocation res1_2 = work.prepared("sel1");
            int tot_trj_num = res1_2.exec().size();
            cout << "tot_trj_num=" << tot_trj_num << endl;
            int tot_viol = 0;
            for(result::size_type trj_num=0; trj_num<tot_trj_num; ++trj_num)
            {
                auto r1 = res1_2.exec();
                int trj_id = r1[trj_num].at(0).as<int>();
                //cout << "trj_id=" << trj_id << endl;
                vector<xywhtss> Trac;

                string select = "s_"+to_string(trj_num);
                Con.prepare(select, "SELECT pnt_id, x, y, time_stamp_ms FROM "+tab1+" WHERE trj_id="+to_string(trj_id)+" ORDER BY time_stamp_ms;");
                prepare::invocation res2 = work.prepared(select);
                int tot_pnt_num = res2.exec().size();
                cout << "trj_num=" << trj_num << "; trj_id=" << trj_id << "; tot_pnt_num=" << tot_pnt_num;
                bool viol = 0;
                for(result::size_type pnt_num=0; pnt_num<tot_pnt_num; ++pnt_num)
                {
                    auto r2 = res2.exec();
                    int pnt_id = r2[pnt_num].at(0).as<int>();
                    bool s = 0; if(mp[pnt_id]){s = 1; viol = 1;}
                    double x = r2[pnt_num].at(1).as<double>();
                    double y = r2[pnt_num].at(2).as<double>();
                    long ts = r2[pnt_num].at(3).as<long>();
                    xywhtss Tr = xywhtss{Point2f(x, y), Point2f(0.01, 0.01), ts, s};
                    Trac.emplace_back(Tr);
                } // -- END for(result::size_type pnt_num=0; pnt_num<tot_pnt_num; ++pnt_num)
                if(viol)
                {Tracs.emplace_back(Trac); tot_viol++;}
                cout << "; viol=" << viol << endl;
             } // -- END for(result::size_type trj_num=0; trj_num<tot_trj_num; ++trj_num)
             cout << "tot_viol=" << tot_viol << " / " << tot_trj_num << endl;
         } // -- END if(!test)
       }
       catch (const sql_error& e)
       {
         cout << "SQL error: " << e.what();
         cout << " Query was: '" << e.query().c_str() << "'";
       }
       cout << "END get_trak_from_sql" << endl;
       return 1;
   } // -- END get_trak_from_sql
}; // -- END class SQL_TEST
