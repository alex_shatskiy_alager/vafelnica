#include <opencv2/core/utility.hpp>
#include "opencv2/highgui.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/imgproc.hpp"

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <chrono>
#include <cmath>
#include <string>
#include <condition_variable>
#include <list>
#include <thread>
#include <future>
#include <dirent.h>
#include <map>
#define OPENCV
#include "../yolo_v2_class.hpp"

using namespace std;
using namespace cv;

//string img_folder_name = "recognition_camera_night";
//string img_folder_name = "recognition_camera_day";
//string img_folder_name = "recognition_camera_morning";
//string img_folder_name = "recognition_camera"; // -- Первый совместный тестовый датасет.
string img_folder_name = "ti_2";

// -- xy - координаты точки середины данной рамки, wh - полуширина и полувысота рамки на текущем кадре с номером n,
// -- s - статус текущей рамки,
// -- h - номер кадра для рамки этого же трека, которая является предыдущей для данной рамки трека,
// -- i - номер предыдущей рамки в кадре с номером h.
struct xywhtss{Point2f xy; Point2f wh; long ts; int s;};

string host_name = "192.168.77.247"; // -- Адрес хоста базы данных.
string db_name = "db_ti_2"; // -- Название бызы данных.
string user_name = "user"; // -- Имя пользователя базы данных.

//   string host_name = "localhost"; // -- Адрес хоста базы данных.
//   string db_name = "example"; // -- Название бызы данных.
//   string user_name = "postgres"; // -- Имя пользователя базы данных.

bool test = 1;
bool dump = 1;
#include "../sql_test_arch.hpp";

string img_way = "/home/alex/QtProg/"+img_folder_name; // -- Альтернатива -- путь к папке с картинками.

// ------------------------------------------------ Параметры для настройки алгоритма:

int fr_h_show = 960; // -- Высота фрейма при показе.

Scalar red = Scalar(0, 0, 255);
Scalar blue = Scalar(255, 0, 0);
Scalar green = Scalar(0, 255, 0);
Scalar white = Scalar(255, 255, 255);
Scalar black = Scalar(0, 0, 0);
Scalar viol = Scalar(255, 0, 255);
Scalar cyan = Scalar(255, 255, 0);
Scalar yell = Scalar(0, 255, 255);
int fr_w0, fr_h0;
int fr_w_show;
float k_show;


bool FileIsExist(string& filePath)
{
    bool isExist = false;
    ifstream fin(filePath.c_str());
    if (fin.is_open()) { isExist = true; }
    fin.close();
    return isExist;
} // -- END FileIsExist




vector<string> dir_content(string& fold, int typ)
{
    vector<string> dc0;
    DIR *dir;
    struct dirent *ent;
    if((dir = opendir(fold.c_str())) != NULL)
    {
      while((ent = readdir(dir)) != NULL)
      {
        string fname = string(ent->d_name);
        // -- typ = 4 (folder), typ = 8 (file).
        if(ent->d_type == typ && fname != "." && fname != ".."){dc0.emplace_back(fname);}
      } // -- END while ((ent = readdir (dir)) != NULL)
      closedir (dir);
    } // -- END if((dir = opendir(way)) != NULL)
    return dc0;
} // -- END dir_content



int main(int argc, char* argv[])
{
   vector<string> img_names;
   vector<long> hist2ms; int img_sz;
   map<long, int> mp;

   img_way += "/";
   img_names = dir_content(img_way, 8);
   cout << "img_names.size()=" << img_names.size() << endl;
   sort(img_names.begin(), img_names.end());
   img_sz = img_names.size();
   hist2ms.resize(img_sz);
   for(int num_fr=0; num_fr<img_sz; ++num_fr)
   {
       string& im0 = img_names[num_fr];
       string im1 = im0.substr(0, im0.length()-4);
       long ts = atol(im1.c_str());
       hist2ms[num_fr] = ts;
       mp[ts] = num_fr;
   } // -- END for(int num_fr=0; num_fr<img_sz; ++num_fr)

   Mat img0 = imread(img_way+img_names[img_names.size()/2]);
   fr_w0 = img0.cols; fr_h0 = img0.rows;
   int dl = 13;
   string s0 = img_names[0]; string im0 = s0.substr(0, s0.length()-4); long ms0 = stol(im0);
   string s1 = img_names[img_sz-1]; string im1 = s1.substr(0, s1.length()-4); long ms1 = stol(im1);
   float fps_midl = (double)img_sz*1000.f/(ms1-ms0);
   cout << "fps_midl=" << fps_midl << endl;
   int img_sz_1 = img_sz-1;

   chrono::duration<double> start_0;
   chrono::system_clock::time_point start_new, start_old;
   float dtime;

   k_show = (float)fr_h_show/fr_h0;
   fr_w_show = round(k_show*fr_w0);

   cout << "fr_w0=" << fr_w0  << "; fr_h0=" << fr_h0 << endl;
   cout << "fr_w_show=" << fr_w_show  << "; fr_h_show=" << fr_h_show << endl;

   Size sz_show = Size(fr_w_show, fr_h_show);
   Mat fr_show = Mat(sz_show, CV_8UC3); fr_show = black;
   Mat frame0;
   int key = 0;
   start_new = chrono::system_clock::now();
   Rect r_black(0, 0, 0.7*fr_w_show, 0.06*fr_h_show);

   vector<vector<xywhtss>> Tracs;
   SQL_TEST st;
   bool get = st.get_trak_from_sql(Tracs);

   bool tr_min = 0;
   for(int num_tr=0; num_tr<Tracs.size(); ++num_tr)
   {
       if(tr_min && num_tr>1){num_tr -= 2;}
       vector<xywhtss>& Trac = Tracs[num_tr];
       for(int num_pt=0; num_pt<Tracs[num_tr].size(); ++num_pt)
       {
          if(tr_min){tr_min=0; num_pt=Tracs[num_tr].size()-1;}
          xywhtss& Tr = Trac[num_pt];
          Point2f& xy = Tr.xy; Point2f& wh = Tr.wh; long& ts = Tr.ts; int& s = Tr.s; int num_fr = mp[ts];
          //cout << "ts=" << ts << endl;
          string im_way = img_way+to_string(ts)+".jpg";
          if(!FileIsExist(im_way)){cout << "num_tr=" << num_tr<< "; num_pt=" << num_pt << "; bad img=" << im_way << endl; continue;}
          frame0 = imread(im_way);
          resize(frame0, fr_show, sz_show);
          fr_show(r_black) = white;

          if(num_pt>0)
          {
              for(int i=0; i<num_pt; ++i)
              {
                  xywhtss& Tr0 = Trac[i]; Point2f& xy0 = Tr0.xy; Point p0 = Point(round(xy0.x*fr_w_show), round(xy0.y*fr_h_show));
                  xywhtss& Tr1 = Trac[i+1]; Point2f& xy1 = Tr1.xy; Point p1 = Point(round(xy1.x*fr_w_show), round(xy1.y*fr_h_show));
                  Scalar clr = blue; if(Tr0.s){clr = red;}
                  circle(fr_show, p0, 4, clr, -1);
                  line(fr_show, p0, p1, green, 2);
              } // -- END for(int i=0; i<num_pt; ++i)
          } // -- END if(num_pt>0)

          Rect rct = Rect(round((xy.x-wh.x)*fr_w_show), round((xy.y-wh.y)*fr_h_show), round(2.f*wh.x*fr_w_show), round(2.f*wh.y*fr_h_show));
          Scalar clr1 = green; Scalar clr2 = blue; if(s==1){clr1 = red; clr2 = red;}
          rectangle(fr_show, rct, clr1, 2);
          circle(fr_show, Point(round(xy.x*fr_w_show), round(xy.y*fr_h_show)), 4, clr2, -1);

          start_old = start_new;
          start_new = chrono::system_clock::now(); start_0 = start_new-start_old;
          dtime = start_0.count();
          float fps_real = 1.f/dtime;
          string txt = "num_tr=" + to_string(num_tr+1) + "/" + to_string(Tracs.size())+"; num_pt="+to_string(num_pt) +"/" + to_string(Tracs[num_tr].size()-1);
          txt += "; num_fr=" + to_string(num_fr) + "/" + to_string(img_sz_1) + "; ts=" + to_string(ts);
          putText(fr_show, txt, Point(10, 40), FONT_ITALIC, 0.8, black, 2, 4);
          try
          {
             imshow(img_folder_name, fr_show); if(num_tr<2){moveWindow(img_folder_name, 10, 40);}
             key = waitKey(0);
             if(key==27){break;}
             if(key=='n')
             {
                 if(num_pt<1){tr_min=1; break;}
                 else{num_pt -= 2;}
             }
             if(key=='z'){if(num_tr>0){num_tr -= 2;} break;}
             if(key=='x'){break;}
          }
          catch(...)
          {cout << endl << "Смени язык клавиатуры на английский!" << endl; num_pt--; continue;}
       } // -- END for(int num_pt=0; num_pt<Tracs[num_tr].size(); ++num_pt)
       if(key==27){break;}
       if(num_tr==Tracs.size()-1){num_tr--;}
   } // -- END for(int num_tr=0; num_tr<Tracs.size(); ++num_tr)

   cout << endl << "END main" << endl;
   return 0;
} // -- END main
