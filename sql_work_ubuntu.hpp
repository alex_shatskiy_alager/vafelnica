#include <pqxx/pqxx>
using namespace pqxx;

class SQL_WORK
{
   public:
    bool put_trak2sql(vector<vector<xyn>>& Tracs, vector<long>& hist2ms)
    {
        cout << endl << "BEGIN put_trak2sql" << endl;
        try
        {
          string connection_string = "host="+host_name+" dbname="+db_name+" user="+user_name;
          connection Con(connection_string.c_str());
          if(Con.is_open()){cout << "DBname= "+db_name+" Exist" << endl;}
          else{cout << "DBname= "+db_name+" NOT Exist!" << endl; return 1;}

          string tab1 = "data_sets"; vector<string> v1 = {"data_set_id", "data_set_name", "data_set_path"};
          string tab2 = "trajectories"; vector<string> v2 = {"trj_id", v1[0], "alg_id"};
          string tab3 = "trajectory_points"; vector<string> v3 = {"pnt_id", v2[0], "x", "y", "time_stamp_ms", "pnt_num"};
          string tab4 = "point_attribute_rectangle"; vector<string> v4 = {"pnt_attr_id", v3[0], "w", "h"};
          int key1_2 = 1; // -- alg_id
          int key3_0 = 1; // -- pnt_attr_id
          int key4_0 = 1; // -- data_set_id
          string data_set_name = img_folder_name;
          string data_set_path = "0";
          nontransaction work(Con);

          Con.prepare("deltab1", "DROP TABLE "+tab1+" CASCADE;");
          string t1 = "CREATE TABLE "+tab1+"("+v1[0]+" SERIAL PRIMARY KEY, "+v1[1]+" VARCHAR(255), "+v1[2]+" VARCHAR(255));";
          Con.prepare("create_tab1", t1);
          Con.prepare("check1", "select exists(select * from information_schema.tables where table_name='"+tab1+"')::int;");

          prepare::invocation res1 = work.prepared("check1");

          if((*(res1.exec()).begin()).at(0).as<int>())
          {
              cout << "Table "+tab1+" EXIST" << endl;
              if(delet){work.prepared("deltab1").exec(); work.prepared("create_tab1").exec();}
          }else{cout << "Table "+tab1+" NOT EXIST & CREATE" << endl; work.prepared("create_tab1").exec();}

          string ins1 = "INSERT INTO "+tab1+"("+v1[1]+", "+v1[2]+") VALUES ('"+data_set_name+"', '"+data_set_path+"');";
          Con.prepare("insert1", ins1);
          work.prepared("insert1").exec();

          Con.prepare("deltab2", "DROP TABLE "+tab2+" CASCADE;");
          string t2 = "CREATE TABLE "+tab2+"("+v2[0]+" SERIAL PRIMARY KEY, "+v2[1]+" int, "+v2[2]+" int";
          t2 += ", CONSTRAINT "+v2[1]+"_fk FOREIGN KEY ("+v2[1]+") REFERENCES "+tab1+"("+v1[0]+") MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION";
          t2 += ");";
          Con.prepare("create_tab2", t2);
          Con.prepare("check2", "select exists(select * from information_schema.tables where table_name='"+tab2+"')::int;");
          prepare::invocation res2 = work.prepared("check2");
          if((*(res2.exec()).begin()).at(0).as<int>())
          {
              cout << "Table "+tab2+" EXIST" << endl;
              if(delet){work.prepared("deltab2").exec(); work.prepared("create_tab2").exec();}
          }
          else{cout << "Table "+tab2+" NOT EXIST & CREATE" << endl; work.prepared("create_tab2").exec();}

          Con.prepare("deltab3", "DROP TABLE "+tab3+" CASCADE;");
          string t3 = "CREATE TABLE "+tab3+" ("+v3[0]+" SERIAL PRIMARY KEY, "+v3[1]+" int, "+v3[2]+" double precision, "+v3[3]+" double precision, "+v3[4]+" bigint, "+v3[5]+" int";
          t3 += ", CONSTRAINT "+v3[1]+"_fk FOREIGN KEY ("+v3[1]+") REFERENCES "+tab2+"("+v2[0]+") MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION";
          t3 += ");";
          Con.prepare("create_tab3", t3);
          Con.prepare("check3", "select exists(select * from information_schema.tables where table_name='"+tab3+"')::int;");
          prepare::invocation res3 = work.prepared("check3");
          if((*(res3.exec()).begin()).at(0).as<int>())
          {
              cout << "Table "+tab3+" EXIST" << endl;
              if(delet){work.prepared("deltab3").exec(); work.prepared("create_tab3").exec();}
          }
          else{cout << "Table "+tab3+" NOT EXIST & CREATE" << endl; work.prepared("create_tab3").exec();}
          Con.prepare("select3", "SELECT "+v3[0]+" FROM "+tab3+" ORDER BY "+v3[0]+" DESC LIMIT 1;");

          Con.prepare("deltab4", "DROP TABLE "+tab4+" CASCADE;");
          string t4 = "CREATE TABLE "+tab4+"("+v4[0]+" SERIAL PRIMARY KEY, "+v4[1]+" int, "+v4[2]+" double precision, "+v4[3]+" double precision";
          t4 += ", CONSTRAINT "+v4[1]+"_fk FOREIGN KEY ("+v4[1]+") REFERENCES "+tab3+"("+v3[0]+") MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION";
          t4 += ");";
          Con.prepare("create_tab4", t4);
          Con.prepare("check4", "select exists(select * from information_schema.tables where table_name='"+tab4+"')::int;");
          prepare::invocation res4 = work.prepared("check4");
          if((*(res4.exec()).begin()).at(0).as<int>())
          {
              cout << "Table "+tab4+" EXIST" << endl;
              if(delet){work.prepared("deltab4").exec(); work.prepared("create_tab4").exec();}
          }
          else{cout << "Table "+tab4+" NOT EXIST & CREATE" << endl; work.prepared("create_tab4").exec();}

          cout << "Tracs.size()=" << Tracs.size() << endl;
          for(int i_tr=Tracs.size()-1; i_tr>-1; --i_tr)
          {
              string ins2 = "INSERT INTO "+tab2+"("+v2[1]+", "+v2[2]+") VALUES ("+to_string(key4_0)+", "+to_string(key1_2)+");";
              Con.prepare("insert2", ins2);
              work.prepared("insert2").exec();

              string s2 = "s2_"+to_string(i_tr);
              Con.prepare(s2, "SELECT "+v2[0]+" FROM "+tab2+" ORDER BY "+v2[0]+" DESC LIMIT 1;");
              prepare::invocation r2 = work.prepared(s2);
              int tr_id = r2.exec().begin().at(0).as<int>();
              vector<xyn>& Trac = Tracs[i_tr];
              cout << "trj_id=" << tr_id << "; Trac.size()=" << Trac.size() << endl;
              for(int k=0; k<Trac.size(); ++k)
              {
                  int pnt_num = k;
                  xyn& Tr = Trac[k];
                  double x = (double)Tr.xy.x/fr_w1;
                  double y = (double)Tr.xy.y/fr_h1;
                  double w = (double)Tr.wh.x/fr_w1;
                  double h = (double)Tr.wh.y/fr_h1;
                  int ind = Tr.n-1; if(ind<0){ind=0;}
                  long ms = hist2ms[num_fr0 + ind];
                  //cout << "Tr.n=" << Tr.n << "; tr_id=" << tr_id << "; pnt_num=" << pnt_num << endl;
                  string ins3 = "INSERT INTO "+tab3+"("+v3[1]+", "+v3[2]+", "+v3[3]+", "+v3[4]+", "+v3[5]+") VALUES ("+to_string(tr_id)+", "+to_string(x)+", "+to_string(y)+", "+to_string(ms)+", "+to_string(pnt_num)+");";
                  string ins_3 = "ins3_"+to_string(i_tr)+"_"+to_string(pnt_num);
                  Con.prepare(ins_3, ins3);
                  work.prepared(ins_3).exec();

                  prepare::invocation res3 = work.prepared("select3");
                  int pnt_id = res3.exec().begin().at(0).as<int>();
                  string ins4 = "INSERT INTO "+tab4+"("+v4[1]+", "+v4[2]+", "+v4[3]+") " "VALUES ("+to_string(pnt_id)+", "+to_string(w)+", "+to_string(h)+");";
                  string ins_4 = "ins4_"+to_string(i_tr)+"_"+to_string(pnt_num);
                  Con.prepare(ins_4, ins4);
                  work.prepared(ins_4).exec();
              } // -- END for(int k=0; k<Trac.size(); ++k)
          } // -- END for(int i_tr=Tracs.size()-1; i_tr>-1; --i_tr)
          if(dump){string dmp = "pg_dump "+db_name+" > ../"+img_folder_name+".dump"; system(dmp.c_str());}
        }
        catch (const sql_error& e)
        {
          cout << "SQL error: " << e.what();
          cout << " Query was: '" << e.query().c_str() << "'";
        }
        cout << "END put_trak2sql" << endl;
        return 1;
    } // -- END put_trak2sql
}; // -- END class SQL_WORK
